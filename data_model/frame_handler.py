__author__ = 'james'


import timeit
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
from database import Database
import json
import dateutil
import datetime
import time
from time import mktime
from datetime import date
import calendar
import uuid
from database import  Database




class DataMiner():
    """
    Class for searching and manipulating  data from a pandas dadaframe
    """

    def __init__(self):
        self.name = 'New Frame handler'

    def get_day(self, d):
        """
        Get the day of a week for a given date.
        :param d: Date as a string
        :return: String
        """
        d = datetime.datetime.strptime(d, "%Y-%m-%d")
        return calendar.day_name[d.weekday()]

    def get_week_number(self, d):
        """
        Get tht week number in the calendar for a given data
        :param d: date
        :return: int
        """
        d = datetime.datetime.strptime(d, "%Y-%m-%d")
        return d.isocalendar()[1]

    def location_tag(self, location):
        upstairs = ['1:32', '1-07R', '1-07L']
        perimeters = ['G-59', 'G-33', 'G-19']  # locations of devices on the perimeter
        canteen = ['Canteen', 'Canteen West']

        return 'p' if location in perimeters \
            else 'u' if location in upstairs \
            else 'c' if location in canteen \
            else 'a' #atrium downstairs

    def col_values_to_list(self, data_frame, col):
        """
        Generate a list of unique items in a data frame column.
        :param dataframe: Pandas dataframe
        :param col: The column to make the list from
        :return: list
        """
        return data_frame[col].unique().tolist()

    def perimeter_only(self, data_frame):
        day = datetime.groupby(['date'])

    def find_path(self, device, day, df):
        day = df[self.df['date'] == 'p']

    def unique(self, arr):
        seen = set()
        seen_add = seen.add
        return [x for x in arr if not (x in seen or seen_add(x))]

    def first_seen(self, df, _id, date=None):
        frame = df[df['mac_id'] == _id]
        min_time = str(frame.time.min())
        return min_time

    def last_time(self, df, _id, date=None):
        frame = df[df['mac_id'] == _id]
        max_time = str(frame.time.max())
        return max_time

    def get_first_and_last(self,df,  _date, device, _location=None):
        """
            Find the first and last times of a device seen on a day in
             the building as a whole or a location
        :param df:
        :param _date:
        :param device:
        :param _location:
        :return:
        """
        frame = df[df['date'] == _date]
        frame = frame[frame['mac_id']== device]
        if _location is not None:
            frame= frame[frame['location']==_location]
        return frame.time.max(), frame.time.min()

    def time_length_in_location(self, df, device, _date, _location='Canteen West'):
        min_time= 600 # 600 seconds

        frame = df[df['date'] == _date]
        frame = frame[frame['mac_id']== device]

    def devices_at_lecture_intervals(self, df, _date, _tag='u'):
        # starting and finishing times in tuples
        times= Utilities().lectue_periods()
        frame = df[df['date'] == _date]
        frame = frame[frame['tag']== _tag]


    def busiest_days(self, df):
        return df.groupby('date').count()

    def busiest_hours(self, df):
        return df.groupby('date').count()

    def busiest_day(self, df):
        return df.groupby('week_day').count()

    def device_walk(self, df, _date='2016-02-29', device='84:38:c6:06:eb'):
        frame = df[df['date'] == _date]
        frame = df[df['mac_id'] == device]
        frame.sort_index(by=['time', 'rssi'], ascending=[True, False])

    def busiest_period(self, df, _date, _location):
        frame = df[df['date'] == _date]
        frame =frame[frame['location'] ==_location]
        return frame.groupby('period').mac_id.apply(lambda x: len(x.unique()))




class Utilities():

    def strip_secs(_time):
        tstamp = _time / np.timedelta64(1, 'm')
        tstamp = int(tstamp)
        tstamp = str(datetime.timedelta(minutes=tstamp))
        tstamp = datetime.datetime.strptime(tstamp, '%H:%M:%S').time()


    def lectue_periods(self):
        start_times=['09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00']
        duration = datetime.timedelta(minutes = 70)
        times=[]
        # starting and finishing times in tuples
        for i in start_times:
            (h, m) = i.split(':')
            start = datetime.timedelta(hours=int(h), minutes=int(m)) - datetime.timedelta(minutes = 5)
            times.append((start,start + duration))

        return times

    def distance(self, _rssi):
        max=100
        # highest 10 is estimates to be 1 rssi == 2 metres
        if _rssi == max:
            return 2
        elif _rssi > 40:
            return int( (max - _rssi ) *.6)
        else:
            return 40

    def hash(self, _mac, _date):

        return uuid.uuid5(uuid.NAMESPACE_DNS,  str(_mac)+ str(_date))

    def period(self, t):
        (h, m, s) = t.split(':')
        h= int(h)
        m=int(m)
        if m >=55 :
            return h +1
        elif m <= 5:
            return h-1

        return h




