__author__ = 'jmt2'
import database
import json
import io


db = database.Database()
data = db.get_all_location_data()




def format_data(data):
    return{
        'device': data[0],
        'rssi': data[1],
        'name': data[2],
        'location' : data[3],
        'epoch': data[4],
        'date': data[5],
        'time': data[6]
    }

def order_data(data, key_position ):
    dict={}
    for d in data:
        key=d[ key_position ]
        if not key in dict:

            dict[key]=[format_data(d)]
        else:
            dict[key].append(format_data(d))

    return dict

def make_json_file(data, fname='data.json'):
    print "Writing to file {}".format(fname)
    with io.open(fname, 'w', encoding='utf-8') as f:
        f.write(unicode(json.dumps(data,  indent=4, ensure_ascii=False)))


def cmp_data(devices, dates, locations ):

    for day in dates:
        print day



def model_room_107 (rooms):
    if '1-07L' in rooms:
        left= rooms['1-07L']
    else:
        return None

    if '1-07R' in rooms:
        right=rooms['1-07R']
    else:
        return None

    tmp=[]
    for lp in left:
        for rp in right:
            date= lp['date']
            if date == rp['date']:
                tmp.append([rp['mac_id', ]])


#mac_id, rssi, name, location, epoch, date_format(datetime,'%Y-%m-%d') as day , date_format(datetime,'%H:%i:%s') as time

if __name__ == '__main__':
    db = database.Database()
    data = db.get_all_location_data()
    data_by_date=order_data(data, 5)
    data_by_device=order_data(data, 0)
    data_by_location= order_data(data, 3)

    model_room_107(data_by_location)
    for r in data_by_location:
        print r






    print len(data)
    print len(data_by_date)
    # print len(data_by_device)
    # print len(data_by_location)





