__author__ = 'james'
import timeit
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
from database import Database
import json
import dateutil
import datetime
import time
from time import mktime
from datetime import date
import calendar
from collections import OrderedDict
from frame_handler import  DataMiner, Utilities

from walk_map import Track

start = int(timeit.default_timer())
MAX_PERIOD_IN_LOCATION = 180  # minutes
RESULT_SET = {}
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


def readings(frame):
    return {
        'rssi': frame[1].rssi.values[0],
        'location': frame[1].location.values[0]
    }


def location_set(frame):
    return {
        'rssi': frame[1].rssi.values[0],
        'location': frame[1].location.values[0],
        'time': frame[1].time.values[0]
    }


def get_daily_packets_series(frame):
    return frame['date'].value_count


def strip_secs(device):
    total_time = (pd.Timestamp(device[1].time.max()) - pd.Timestamp(device[1].time.min()))
    tstamp = total_time / np.timedelta64(1, 'm')
    tstamp = int(tstamp)
    tstamp = str(datetime.timedelta(minutes=tstamp))
    tstamp = datetime.datetime.strptime(tstamp, '%H:%M:%S').time()
    return str(tstamp)


def str_to_time(str):
    """
    convert strein to datetime object hh:mm:ss
    :rtype : object
    """
    return datetime.datetime.strptime(str, '%H:%M:%S').time()


def str_to_date(str):
    """
    Convert string to date object yy-mm-dd
    :rtype : object
    """
    return datetime.datetime.strptime(str, "%Y-%m-%d").date()


def get_day(d):
    """
        Get the day of a week for a given date.

    :param d: Date as a string
    :return: string
    """
    d = datetime.datetime.strptime(d, "%Y-%m-%d")
    return calendar.day_name[d.weekday()]


def get_week_number(d):
    """
    Get the week number of a given date
    :param d: Date as a string
    :return: int
    """
    d = datetime.datetime.strptime(d, "%Y-%m-%d")
    return d.isocalendar()[1]


class PacketFrame():
    """
    The frame containing the relevant data from the database
    """

    weekend = ['saturday', 'sunday']  # days of the weekend
    rm_107 = ['1-07L', '1-07R', ]
    upstairs = ['1:32'].extend(rm_107)
    perimeters = ['G-59', 'G-33', 'G-19']  # locations of devices on the perimeter

    def __init__(self, df=None):

        # Set up the data frame
        miner = DataMiner()

        if df is None:
            self.df = df if df is not None else  pd.read_sql_query(sql=Database().sql, con=Database().db_connect(),
                                                                   index_col=None, coerce_float=True, params=None,
                                                                   parse_dates=None, chunksize=None)

            # Add  new cols, week number, day of the week  and a tag for device location.
            print 'doing lambda weekday'
            self.df['week_day'] = self.df['date'].map(lambda x: miner.get_day(x))
            print 'doing lambda week'
            self.df['week'] = self.df['date'].map(lambda x: miner.get_week_number(x))
            print 'doing lanbda tag'
            self.df['tag'] = self.df['location'].map(lambda x: miner.location_tag(x))
            print 'distance'
            self.df['distance'] = self.df['rssi'].map(lambda x: Utilities().distance(x))
            print 'period'
            self.df['period'] = self.df['time'].map(lambda x: Utilities().period(x)) # hourly period

            #self.df['hash'] = self.df['mac_id'].map(lambda x: Utilities().hash(x, df['date']))

        else:
            self.df = df
        self.clients = miner.col_values_to_list(self.df, 'mac_id')  # list of devices
        self.dates = miner.col_values_to_list(self.df, 'date')  # list of dates
        self.locations = miner.col_values_to_list(self.df, 'location')  # list of locations

        self.aggregations = {
            # 'time': {
            #     'earliest': 'min',
            #     'latest': 'max',
            # },
            'mac_id': {'count': 'count'},
            #
            # 'location':{
            #     'time': 'count'
            # },
            # # 'rssi':{
            # #     'rssi':'rssi'
            # # }
        }


    def busiest_week(self):
        """
        The weeks with the ttoal count of devices seen
        :return:
        """
        print "Busiest week"
        return self.df.groupby('week')['mac_id'].count()

    def busiest_days_of_the_week(self):
        print "busiest_days_of_the_week"
        return self.df.groupby(['week', 'week_day'])['mac_id'].count()

    def avg_on_day(self, day=None):
        print "avg_on_day"
        if day is None:
            return self.df.groupby(['week_day'])['mac_id'].count().mean()
        else:
            return self.df.groupby(['week_day'])['mac_id'].sum()

    def aggregate(self):
        return self.df.groupby(['date']).agg(self.aggregations)

    def perimeter_devices(self):
        print 'perimiter devices'
        f2 = self.df[self.df['tag'] == 'p']
        perimiters = f2[['date', 'mac_id', 'tag']]
        return perimiters

    def internal_devices(self):
        print 'internal devices'
        f2 = self.df[self.df['tag'] != 'p']
        internal = f2[['date', 'mac_id', 'tag']]
        return internal

    def daily_perimiter_only(self, perimeter, internal, sort=True):
        """

        :param perimeter: Dataframe with all devices on the perimeter
        :param internal: Dataframe with all devices  seen internally
        :return: dictionary with dates as keys to arrays containing the devices seen on the perimeter only
        """
        print 'Perimeter Only'
        perimeter_dates = perimeter['date'].unique().tolist()
        # new dictionary to return
        perimeter_only = dict()
        for pd in perimeter_dates:
            # make new dataframes with current days devices
            date_df = perimeter[perimeter["date"] == pd]
            tempdf = internal[internal["date"] == pd]
            # list of devices  seen internally in building
            mac_list = tempdf['mac_id'].unique().tolist()
            # for each mac in todays list
            for row in date_df.iterrows():
                data = row[1]
                if data['mac_id'] not in mac_list:
                    if pd not in perimeter_only:
                        perimeter_only[pd] = []
                    if data['mac_id'] not in perimeter_only[pd]:
                        perimeter_only[pd].append(data['mac_id'])

        return perimeter_only if not sort else OrderedDict(sorted(perimeter_only.items(), key=lambda t: t[0]))






    def lacation_busiest_period(self):
            dates = self.df['date'].unique().tolist()
            locations= self.df['location'].unique().tolist()

            for d in dates:
                for l in locations:
                    data=DataMiner().busiest_period( df, d, l)
                    write_to_text(data, 'On {} in {}'.format(d,l))
                    print d, l ,'\n', data




            # print row[1]['mac_id']




            # print 'd', type(d)
            # print "(perimeter['date'].dtype)", (perimeter['date'].dtype)
            # print "perimeter['date']", type(perimeter['date']), type(perimeter)
            # date_df = perimeter[perimeter['date']== d]
            # print perimeter['date'],
            # print date_df
            # print date_df, type(date_df)  #<class 'pandas.core.series.Series'>
            # print date_df.values
            #
            # ids_seen = set()
            # for row in date_df.iterrows():
            #     if row["mac_id"] not in ids_seen:
            #        print  ' Enter nested loop and do some stuff for each unique id. Add it to a set so we can keep track '
            #     else:
            #         print 'looping rows'
            #
            #     ids_seen.add(row["id"])
            #
            # print ids_seen

    def intersection(self, s1, s2):
        print 'intersection'
        return pd.Series(np.intersect1d(s1, s2))

        # def ts(self, tstamp):
        #     print tstamp, type(tstamp)
        #     t=datetime.datetime.strptime(tstamp, '%H:%M:%S').time()
        #     print t
        #     return tstamp


def runtime(_start=start, _stop=timeit.default_timer()):
    """
        Calculate the runtime of the program
    """
    run = int(_stop) - _start
    m = int(run / 60)
    h = int(m / 60)
    m -= h * 60
    s = (int)(run - (m * 60))
    return str_to_time("{}:{}:{}".format(h, m, s))

def write_to_text(data, info):

    with open("Output.txt", "w") as text_file:
        text_file.write("{}\n {}\n".format(info, data))


if __name__ == '__main__':

    df = pd.read_csv('~/packets.csv')
    frame = PacketFrame(df)
    # fh= DataMiner()
    # print m.df.head()
    # print m.locations
    # print frame.busiest_week()
    # write_to_text(frame.busiest_week(), 'Busiest Week')
    # write_to_text(frame.busiest_days_of_the_week(), 'Busiest Days of the Week')
    # print frame.busiest_days_of_the_week()
    #frame.lacation_busiest_period(frame.df, )


    # print m.busiest_days_of_the_week()
    # print m.avg_on_day()
    # print m.perimeter_devices()
    # print m.internal_devices()
    # print m.df

    # print json.dumps(m.daily_perimiter_only(m.perimeter_devices(), m.internal_devices() ) ,indent=4, sort_keys=True, ensure_ascii=False)
    # print m.intersection(m.internal_devices(), m.perimeter_devices())
    # m.daily_perimiter_only(m.perimeter_devices(), m.internal_devices())
    # print fh.get_first_and_last(df, '2016-02-14', '70:cd:60:fe:c5:64' )
    # print frame.df.head(60)
    #
    # print fh.busiest_day(frame.df)
    # print fh.busiest_days(frame.df)




    # p=Phone()
    # print p.get_frame().head()
    #
    # print p.do_data()

    # m=PacketModel()
    # agg =  m.aggregate()
    # print agg
    # print type(agg)
    # print agg.keys()

    # agg.plot(y='count'[1]);

    # x=pd.DataFrame(daily_packet_count.to_records())
    # print x.keys()
    # plt.interactive(False)
    # daily_packet_count.plot( );
    # pl
    #
    # points = np.arange(-5, 5, 0.01)
    # dx, dy = np.meshgrid(points, points)
    # z = (np.sin(dx)+np.sin(dy))
    # plt.imshow(z)
    # plt.colorbar()
    # plt.title('test')
    # plt.show()
    #
    # print daily_packet_count, type(daily_packet_count)


    # df = pd.DataFrame({'date':['2015-01-01','2015-01-02','2015-01-03','2015-01-01','2015-01-02','2015-01-03']})


    # print df.head()
    # print df.tail()

    print 'runtime {}'.format(runtime(_stop=timeit.default_timer()))
    #frame.df.to_csv('~/packets.csv')
    t = Track(frame.df)
    t.walk()
