__author__ = 'james'
import timeit
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
from database import Database
import json
import dateutil
import datetime
import time
from time import mktime
from datetime import date
import calendar
from collections import OrderedDict
from frame_handler import  DataMiner

start = int(timeit.default_timer())
MAX_PERIOD_IN_LOCATION = 180  # minutes
RESULT_SET = {}
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


def readings(frame):
    return {
        'rssi': frame[1].rssi.values[0],
        'location': frame[1].location.values[0]
    }


def location_set(frame):
    return {
        'rssi': frame[1].rssi.values[0],
        'location': frame[1].location.values[0],
        'time': frame[1].time.values[0]
    }


def get_daily_packets_series(frame):
    return frame['date'].value_count


def strip_secs(device):
    total_time = (pd.Timestamp(device[1].time.max()) - pd.Timestamp(device[1].time.min()))
    tstamp = total_time / np.timedelta64(1, 'm')
    tstamp = int(tstamp)
    tstamp = str(datetime.timedelta(minutes=tstamp))
    tstamp = datetime.datetime.strptime(tstamp, '%H:%M:%S').time()
    return str(tstamp)


def str_to_time(str):
    """
    convert strein to datetime object hh:mm:ss
    :rtype : object
    """
    return datetime.datetime.strptime(str, '%H:%M:%S').time()


def str_to_date(str):
    """
    Convert string to date object yy-mm-dd
    :rtype : object
    """
    return datetime.datetime.strptime(str, "%Y-%m-%d").date()


def get_day(d):
    """
        Get the day of a week for a given date.

    :param d: Date as a string
    :return: string
    """
    d = datetime.datetime.strptime(d, "%Y-%m-%d")
    return calendar.day_name[d.weekday()]


def get_week_number(d):
    """
    Get the week number of a given date
    :param d: Date as a string
    :return: int
    """
    d = datetime.datetime.strptime(d, "%Y-%m-%d")
    return d.isocalendar()[1]


# Extract data from database...


# Add a day of the week col
# df['week_day'] = df['date'].map(lambda x: get_day(x) )
# df['week_number']= df['date'].map(lambda x: get_week_number(x))


# df=Database().get_all_location_data()
# by_mac = df.groupby('date')
# print by_mac.count()
# print df.info()

# print df
#
# df.filter(items=None, like=None, regex=None, axis=None)

# frame headers
# mac_id, rssi, name, location, epoch, date , time

# devices=one = df.groupby('mac_id')
#
# print df
# unique_client = pd.unique(df.mac_id.ravel())
# #df2=df[df["mac_id"] == "79f64138-d332-51c8-a583-686f30eb65f9" ]
#
#
# #df2=df.sort(columns=['date', 'mac_id', 'location', 'time'])
# df2=df.sort_values(by=['date', 'mac_id', 'location', 'time'])
#
#
# date_groups = df2.groupby(['date'])
#
# clients=df2['mac_id'].unique().tolist()
# dates=df2['date'].unique().tolist()
# locations=df2['location'].unique().tolist()
#
#
# daily_packet_count = df2['date'].value_counts() # series with dates and num packets for each day
# day_with_max_packets= daily_packet_count.idxmax(),daily_packet_count.max() # Max packets in a single day
# average_packets_per_day= daily_packet_count.mean() # average packets per day
#
#
# print daily_packet_count, type(daily_packet_count)
# print 'Max count', day_with_max_packets, type(day_with_max_packets)
# #average_packets_each_day=daily_packet_count = df2['date']['mac_id'].value_counts()
# #print average_packets_per_day
# #print average_packets_each_day
#
# day_mac=  df2.groupby(['date', 'mac_id'])['mac_id'].count() #df2.groupby('date')['mac_id']
# print type(day_mac)
#
#
#
#
# #df2.groupby(['date']).agg(aggregations)
#
# RESULT_SET['total_packets']=len(df2)
# for d in dates:
#
#     today= date_groups.get_group(d) # each days data
#     RESULT_SET[d] ={}
#     # Number of packets seen today
#     RESULT_SET[d]['num_packets']=len(today)
#
#     ds=today['mac_id'].unique().tolist()
#     devices=  today.groupby(['mac_id'])
#
#
#     # Number of devices seen today
#     RESULT_SET[d]['num_devices'] = len(today.mac_id.unique())
#     RESULT_SET[d]['devices'] =  {}
#
#
#     for device in devices:
#         #todays_devices=today.get_group('device')
#         #RESULT_SET[d]['devices'][device[0]] ={}
#
#         # RESULT_SET[d]['num_devices'] = len(device[1].location)
#
#         #RESULT_SET[d]['device'] = 'd'
#
#         #RESULT_SET[d]['num_packets']=device[1].count()
#         #print device[0], num_of_packets
#         times= device[1].groupby(['time'])
#         locations=device[1].groupby(['location'])
#
#         #
#         if device[0]=='84:38:38:c6:06:eb':
#
#
#             dev=device[0]
#             #print dev
#             # RESULT_SET[d]['devices'][dev]={'readings':{}}
#             RESULT_SET[d]['devices'][dev]={'locations':{}}
#
#
#             #RESULT_SET[d]['devices'][dev]={'readings':{}}
#             #print RESULT_SET[d]['pis']
#             ## if d in RESULT_SET[d]['pis'].keys():
#             #    print d
#             #RESULT_SET[d]['devices']['84:38:38:c6:06:eb'] =  '111'
#         #     #RESULT_SET[d]['devices']['dev'] =d
#         #     #RESULT_SET[d]['devices'].append( device[0] )
#         #
#             device_data=device[1]
#             min_time= str(device_data.time.min())
#             max_time= str(device_data.time.max())
#             total_time = strip_secs(device)
#
#
#
#             #test= time.strftime("%H:%M:%S", time.gmtime(test))
#             #print test, type(test)
#
#             RESULT_SET[d]['devices'][dev]['first-seen'] = min_time
#             RESULT_SET[d]['devices'][device[0]]['last_seen'] = max_time
#             RESULT_SET[d]['devices'][device[0]]['total_time'] = total_time
#             RESULT_SET[d]['devices'][device[0]]['num_packets']=len(device[1])
#
#
#             # for time in times:
#             #     RESULT_SET[d]['devices'][dev]['readings'][time[0]]=readings(time)
#             #     locs=time[1].groupby(['location'])
#
#
#
#             for l in locations:
#                 location_devices= device[1].groupby(['location'])
#                 #print "location devices", len(location_devices), type(location_devices)
#                 for d1 in location_devices:
#                     frm=d1[1]
#
#                 #print d #, l, len(location_devices), type(location_devices)
#                 #print 'Checkng {}'.format(l[0])
#
#                 if  l[0] in RESULT_SET[d]['devices'][dev]:
#
#                     RESULT_SET[d]['devices'][dev][l[0]]['data'].append(location_set(l) )
#
#                     RESULT_SET[d]['devices'][dev][l[0]]['packets']+=1
#                     #print 'Its there {}'.format(l[0])
#
#                 else:
#                     RESULT_SET[d]['devices'][dev][l[0]]={}
#                     RESULT_SET[d]['devices'][dev][l[0]]['data']=[]
#                     RESULT_SET[d]['devices'][dev][l[0]]['data'].append(location_set(l) )
#                     RESULT_SET[d]['devices'][dev][l[0]]['packets']=1
#                     #print 'Its not there {}'.format(l[0])

# print 'Next Locartion'
# print l
# readings
# for t in time:
#     test = t.index
#     # RESULT_SET[d]['devices'][dev]['readings'][time[0]]['location'] = t


# RESULT_SET[d][device[0]][t[1]]={t[1]}
# locations= device[1].groupby(['location'])
# for l in locations:
#
#
#   print 'Locations\n', l[1]
# for x in t[1]:
#     print type(x)




# print json.dumps(RESULT_SET,indent=4, sort_keys=True, ensure_ascii=False)
# d= json.dumps(RESULT_SET,indent=4, ensure_ascii=False)
#
# with open('data.txt', 'w') as outfile:
#     json.dump(RESULT_SET, outfile, ensure_ascii=False, sort_keys = True, indent=4 )
#

# print len(df2)

class JsonBuilder():
    """
    Make a json file from a data frame
    """

    def __init__(self, dframe, devices=None):
        self.df = dframe
        self.macs = devices if devices is None else self.dframe['mac_id'].unique().tolist()

    def make_file(self):
        date_groups = self.df.groupby(['date'])
        for d in self.df['date'].unique():
            today = date_groups.get_group(d)  # each days data
            RESULT_SET[d] = {}
            # Number of packets seen today
            RESULT_SET[d]['num_packets'] = len(today)

            ds = today['mac_id'].unique().tolist()
            devices = today.groupby(['mac_id'])


            # Number of devices seen today
            RESULT_SET[d]['num_devices'] = len(today.mac_id.unique())
            RESULT_SET[d]['devices'] = {}

            for device in devices:
                times = device[1].groupby(['time'])
                locations = device[1].groupby(['location'])

                if device[0] in self.macs:

                    dev = device[0]
                    RESULT_SET[d]['devices'][dev] = {'locations': {}}
                    device_data = device[1]
                    min_time = str(device_data.time.min())
                    max_time = str(device_data.time.max())
                    total_time = strip_secs(device)

                    RESULT_SET[d]['devices'][dev]['first-seen'] = min_time
                    RESULT_SET[d]['devices'][device[0]]['last_seen'] = max_time
                    RESULT_SET[d]['devices'][device[0]]['total_time'] = total_time
                    RESULT_SET[d]['devices'][device[0]]['num_packets'] = len(device[1])


                    # for time in times:
                    #     RESULT_SET[d]['devices'][dev]['readings'][time[0]]=readings(time)
                    #     locs=time[1].groupby(['location'])



                    for l in locations:
                        location_devices = device[1].groupby(['location'])
                        for d1 in location_devices:
                            frm = d1[1]

                        if l[0] in RESULT_SET[d]['devices'][dev]:

                            RESULT_SET[d]['devices'][dev][l[0]]['data'].append(location_set(l))

                            RESULT_SET[d]['devices'][dev][l[0]]['packets'] += 1
                            # print 'Its there {}'.format(l[0])

                        else:
                            RESULT_SET[d]['devices'][dev][l[0]] = {}
                            RESULT_SET[d]['devices'][dev][l[0]]['data'] = []
                            RESULT_SET[d]['devices'][dev][l[0]]['data'].append(location_set(l))
                            RESULT_SET[d]['devices'][dev][l[0]]['packets'] = 1
                            # print 'Its not there {}'.format(l[0])

    def file_writer(self):
        """
            Write dictionary to file
        """
        with open('data.txt', 'w') as outfile:
            json.dump(RESULT_SET, outfile, ensure_ascii=False, sort_keys=True, indent=4)



