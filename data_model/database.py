__author__ = 'james'
import MySQLdb
from datetime import datetime, date
import time
import json
from time import mktime


# SELECT mac_id, rssi,name,location, epoch, date_format(datetime,'%Y-%m-%d') as day ,  date_format(datetime,'%H:%i:%s') as time   FROM packet_locations ;
# select mac_id, count(*) as count, date_format(datetime,'%Y-%m-%d') as day, date_format(datetime,'%H:%i:%s') as time, pi_client_id as location from raspberry_packets_packet group by mac_id, day, location, time;

"""

CREATE OR REPLACE VIEW ignore_list AS
SELECT distinct(mac_id) as device
FROM raspberry_packets_packet
WHERE TIME(date) > "23:00:00" OR TIME(date) < "07:00:00";

SELECT mac_id, rssi, name, location, epoch, date_format(datetime,'%Y-%m-%d') as day ,
                date_format(datetime,'%H:%i:%s') as time   FROM packet_locations
                WHERE mac_id NOT IN ( SELECT device FROM ignore_list);
"""


class Database():

    #sql="SELECT mac_id, rssi, location, date_format(datetime,'%Y-%m-%d') as day , date_format(datetime,'%H:%i:%s') as time   FROM packet_locations ;"
    sql="SELECT mac_id, rssi, name, location, epoch, date_format(datetime,'%Y-%m-%d') as date ,  " \
                "date_format(datetime,'%H:%i:%s') as time   FROM packet_locations " \
                "WHERE mac_id NOT IN ( SELECT device FROM ignore_list)";




    def __init__(self, host="localhost", database="raspberry_packets",  user="raspberry", passwd="raspberry11"):
            self.host = host
            self.database=database
            self.user=user
            self.passwd=passwd


    def make_view(self):
        print  "Creating view ignore_list"
        sql="CREATE OR REPLACE VIEW ignore_list " \
            "AS SELECT distinct(mac_id) as device " \
            "FROM raspberry_packets_packet WHERE TIME(date) > \"23:00:00\" OR TIME(date) < \"07:00:00\";"
        db = self.db_connect()
        cursor = db.cursor()
        cursor.execute(sql)
        db.close()

    def db_connect(self):
        return MySQLdb.connect( self.host, self.user, self.passwd, self.database)

    def get_all_dates(self, minDate=None, maxDate=None):

        if minDate is None and maxDate is None:
            sql="SELECT  date_format(datetime,'%Y-%m-%d %k:') AS dates  " \
                "FROM packet_locations  ;"
        elif minDate is None and maxDate is not  None:
            maxDate= " '%s' " % maxDate
            sql="SELECT  date_format(datetime,'%Y-%m-%d %k:') AS dates  " \
                "FROM packet_locations WHERE dates < " + maxDate +" ;"

        elif maxDate is None and minDate is not None:
            minDate= " '%s' " % minDate
            sql="SELECT  date_format(datetime,'%Y-%m-%d %k:') AS dates  " \
                "FROM packet_locations WHERE dates  > " + minDate +" ;"

        else:
            maxDate= " '%s' " % maxDate
            sql="SELECT  date_format(datetime,'%Y-%m-%d %k:') AS dates  " \
                "FROM packet_locations " \
                "WHERE dates BETWEEN  " + minDate +" AND  " + maxDate +" ;"


        data=[]
        try:
            db = self.db_connect()
            cursor = db.cursor()
            cursor.execute(sql)
            results = cursor.fetchall()
            num=0

            for row in results:
                data.append([ row[1], row[3]]) #  "count": row[3]}
                num+=1

        except MySQLdb.Error, e:
            print e


            print "Error {}: {}" .format(e.args[0], e.args[1])
        finally:
            if db:
                db.close()
            return data

    def get_all_location_data(self, location=None):
        dict={}
        db=MySQLdb.connect( self.host, self.user, self.passwd, self.database)
        cursor = db.cursor()
        results=None
        try:

            sql="SELECT mac_id, rssi, name, location, epoch, date_format(datetime,'%Y-%m-%d') as day ,  " \
                "date_format(datetime,'%H:%i:%s') as time   FROM packet_locations " \
                "WHERE mac_id NOT IN ( SELECT device FROM ignore_list)";

            cursor.execute(sql)
            results = cursor.fetchall()

        except MySQLdb.Error, e:


            print "Error {}: {}" .format(e.args[0], e.args[1])
        finally:
            if db:
                db.close()

        return results


    def query(self, _location, _date):
        _location= " '%s' " % _location
        _date = " '%s' " % _date
        # SELECT mac_id, date_format(datetime,'%Y-%m-%d') as date , location,  count(*) as counter
        # FROM packet_locations WHERE  mac_id NOT IN ( SELECT device FROM ignore_list) Group by date, location, mac_id;
        # SELECT mac_id, rssi, name, location, epoch, concat( date_format(datetime,'%Y-%m-%d %k:') ,
        # lpad(floor(minute(datetime)/60)*60,2,'0'), ':00') datetime, count(distinct(mac_id)), date_format(datetime,'%Y-%m-%d') as date ,
        # date_format(datetime,'%H:%i:%s') as time  FROM packet_locations WHERE  mac_id NOT IN ( SELECT device FROM ignore_list)
        #  group by date(datetime), hour(datetime), floor(minute(datetime)/60), location ;

        return "SELECT mac_id, rssi, name, location, epoch, date_format(datetime,'%Y-%m-%d') as date ,  " \
                "date_format(datetime,'%H:%i:%s') as time ,count(*) as counter  FROM packet_locations " \
                "WHERE location = _location and date = _date and and mac_id NOT IN ( SELECT device FROM ignore_list)";


    def re_hash(self):
        mac_regx = re.compile(r'^([0-9A-F]{1,2})' + '\:([0-9A-F]{1,2})'*5 + '$', re.IGNORECASE)