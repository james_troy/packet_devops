# Data Model

Data Model is a script that reads all the data from the database into a pandas dataframe.

> The aim of the script is to manipulate the data to model human behaviour and   
> show frequencies of the data associated with Wi-Fi enabled devices

*Written in Markdown!*

### Version
1.0.0
