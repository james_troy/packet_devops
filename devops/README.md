# Devops

Devops is a script that uses [Ansible] to set up and automate any updates.


> The aim of the script is to initially automate the set up and deployment of each Raspberry Pi and then 
> to push updates to each device as changes are made

*Written in Markdown!*

### Version
1.0.0


[Ansible]: <https://www.ansible.com>